package calc;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class GUI extends ParseText implements ActionListener { 
    
	static JTextField input;  
    static JFrame frame; 
    static JButton button;  
    static JLabel display;
    GUI(){
    	build();
    }
    
	public static void build() {
  		frame = new JFrame ("Derivative Calculator");
  		display = new JLabel(" Enter your formula above: ");
  		button = new JButton("Execute");
  		input = new JTextField(16);
  		
  		frame.setSize(300,500);
  		
  		JPanel panel = new JPanel();
  		panel.setLayout(new BorderLayout());
  		panel.add(input, BorderLayout.NORTH);
  		panel.add(display,BorderLayout.CENTER);
  		panel.add(button, BorderLayout.SOUTH);
  		frame.add(panel);
  		
  		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  		frame.setVisible(true);
	}
		
  	// if the button is pressed 
      public void actionPerformed(ActionEvent event) 
      { 
          String s = event.getActionCommand(); 
          if (s.equals("Execute")) { 
        	  
        	  setInput(input.getText()); // Takes user input and sets variable in ParseText for further use
              display.setText(String.valueOf(getResult())); // set the text of the display...Change from double to string
              input.setText("  "); // set the text of field to blank
          } 
      } 
}