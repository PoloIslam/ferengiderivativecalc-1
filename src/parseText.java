package calc;

public class ParseText {
	/* 
	 * Stores user input into string variable for later use
	 * depending on how we want to proceed with the input
	 * The GUI will call setInput to set variable and
	 * will call getResult to display result.
	 */
	
	private String input;
	private double result; 
	private int inputLength;
	
	public void setInput(String input) {
		this.input = input;
		/* takes input from GUI, sends to shunting yard, 
		 * result of shunting yard is computed in evalRPN 
		 * and variable "result" is set.
		 * GUI calls getResult() and result is displayed
		 */
		setResult(Compute.evalRPN(new ShuntingYardAlg().makePF(input))); 
		setLength();
	}
	
	public void setLength() {
		inputLength = input.length();
	}
	
	public void setResult(double result) {
		this.result = result;
	}
	
	public String getInput() {
		return input;
	}
	
	public int getLength() {
		return inputLength;
	}
	
	public double getResult() {
		//will display into GUI as result;
		return result;
	}
}
