package calc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class customFrame {

	private JFrame customFrame;

	/**
	 * Launch the application.
	 */
	public static void customWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					customFrame window = new customFrame();
					window.customFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public customFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		customFrame = new JFrame();
		customFrame.setTitle("Customize");
		customFrame.setBounds(100, 100, 270, 155);
		customFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		customFrame.getContentPane().setLayout(null);
		
		//Label info
		JLabel backgroundLabel = new JLabel("Background Color:");
		backgroundLabel.setBounds(6, 17, 131, 16);
		customFrame.getContentPane().add(backgroundLabel);
		
		JLabel fontLabel = new JLabel("Font Color:");
		fontLabel.setBounds(6, 60, 114, 16);
		customFrame.getContentPane().add(fontLabel);
		
		JComboBox fontColorBox = new JComboBox();
		fontColorBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//...drop down menu selection for font color
			}
		});
		
		JComboBox backgroundColorBox = new JComboBox();
		backgroundColorBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//...drop down menu selection for background color
			}
		});
		backgroundColorBox.setBounds(125, 13, 139, 27);
		customFrame.getContentPane().add(backgroundColorBox);
		fontColorBox.setBounds(125, 56, 139, 27);
		customFrame.getContentPane().add(fontColorBox);
		
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//...save changes
			}
		});
		saveButton.setBounds(16, 98, 117, 29);
		customFrame.getContentPane().add(saveButton);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//...cancel changes
			}
		});
		cancelButton.setBounds(135, 98, 117, 29);
		customFrame.getContentPane().add(cancelButton);
	}

}
