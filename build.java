package calc;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class build extends holdText {

	private JFrame calcFrame;
	private JTextField input;
	private JTextField answer;
	
	//Launch the application.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					build window = new build();
					window.calcFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Create the application.
	public build() {
		initialize();
	}

	//Initialize the contents of the frame.
	private void initialize() {
		calcFrame = new JFrame();
		calcFrame.setTitle("Ferengi Calculator");
		calcFrame.setBounds(100, 100, 340, 200);
		calcFrame.setBackground(null);
		calcFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calcFrame.getContentPane().setLayout(null);
		calcFrame.setBackground(Color.WHITE);
		
		//Panel info
		input = new JTextField();
		input.setHorizontalAlignment(SwingConstants.TRAILING);
		input.setBounds(6, 25, 313, 26);
		calcFrame.getContentPane().add(input);
		input.setColumns(10);
		
		JEditorPane inputPane = new JEditorPane();
		inputPane.setBounds(6, 22, 313, 29);
		calcFrame.getContentPane().add(inputPane);
		
		answer = new JTextField();
		answer.setHorizontalAlignment(SwingConstants.TRAILING);
		answer.setBounds(6, 82, 313, 26);
		calcFrame.getContentPane().add(answer);
		answer.setColumns(10);
		
		JEditorPane answerPane = new JEditorPane();
		answerPane.setBounds(6, 80, 313, 29);
		calcFrame.getContentPane().add(answerPane);
		
		//enterInput Label info
		JLabel enterInputLabel = new JLabel("Enter Formula:");
		enterInputLabel.setBounds(6, 6, 117, 16);
		enterInputLabel.setForeground(Color.BLACK);
		calcFrame.getContentPane().add(enterInputLabel);
		
		//example Label info
		JLabel exampleLabel = new JLabel("ex. ((-3)*(5+8))-10)/5");
		exampleLabel.setFont(new Font("Lucida Grande", Font.ITALIC, 10));
		exampleLabel.setBounds(202, 54, 117, 16);
		exampleLabel.setForeground(Color.BLACK);
		calcFrame.getContentPane().add(exampleLabel);
		
		//answer Label info
		JLabel answerLabel = new JLabel("Answer:");
		answerLabel.setBounds(6, 63, 61, 16);
		answerLabel.setForeground(Color.BLACK);
		calcFrame.getContentPane().add(answerLabel);
		
		//execute button info
		JButton executeButton = new JButton("Execute");
		executeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setInput(input.getText());
				answer.setText(String.valueOf(getResult()));
			}
		});
		executeButton.setBounds(0, 120, 109, 29);
		calcFrame.getContentPane().add(executeButton);
		
		//reset button info
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input.setText("");
				answer.setText("");
			}
		});
		resetButton.setBounds(108, 120, 109, 29);
		calcFrame.getContentPane().add(resetButton);
		
		//customize button info
		JButton customizeButton = new JButton("Customize");
		customizeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc.customFrame.customWindow();
			}
		});
		customizeButton.setBounds(216, 120, 109, 29);
		calcFrame.getContentPane().add(customizeButton);
	}
}